<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Q5+DA}u&w@fUyOU,=8Nn=c2+phEJGVmakH pI&:({(7Z3<mC?y{(?*Cp#W?MC Qd');
define('SECURE_AUTH_KEY',  'I6F&q.!3FOTn>>!M@aOS;5jcv;)k4Rt_Ojp)M;=QH-G9JaV6ah8n(R`p&_@ :qV@');
define('LOGGED_IN_KEY',    ',1i/+9j1#+$hR=1>puWTMO@}962O- M{k,Dpp!fP6fJQv|J]0x)W/F)siXFI|01*');
define('NONCE_KEY',        'ebkqgHnaG+G3q%d/GM.*-?r[*qtw3l~+U^Y@K+z./I]-$|Ad6NLzfQx~G(G$]2%`');
define('AUTH_SALT',        '~-E.NkuG:5>`(iuU|edOe;[u79L%xdx`H8`/L0.`v=FStQ&WSYBH?%qLLUlF1{[H');
define('SECURE_AUTH_SALT', 'X2Q_Ufi/~N@7<%rflI~bAiy`/wu(?MGV}VbU$E6mW=c(zdme4oV@K;b$fl91`:Qp');
define('LOGGED_IN_SALT',   'I(SK9p8{T4<>OCA/Tjvjit@:%T$Z 5[jcy3~Id<Z*@WvsY&2@qH?7*t?<yEof-NJ');
define('NONCE_SALT',       'gMt8_yK:wZxZTy?b3j/YwAIGa4(spG9[q5_B=A@o;wyb8b,0e=`qppuMD{%Hn;nc');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
